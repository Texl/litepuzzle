﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace LitePuzzle
{
    public sealed class NodeState
    {
        public NodeState(int number, int nextNeighborIndex, int[] neighbors, int bit)
        {
            Number = number;
            NextNeighborIndex = nextNeighborIndex;
            Neighbors = neighbors;
            Bit = bit;
        }

        public readonly int Number;
        public readonly int NextNeighborIndex;
        public readonly int[] Neighbors;
        public readonly int Bit;
    }

    public sealed class PuzzleState
    {
        public PuzzleState(Dictionary<int, NodeState> nodeStates)
        {
            NodeStates = nodeStates;
        }

        public PuzzleState WithNodeReplaced(NodeState nodeState) =>
            new PuzzleState(NodeStates.ToDictionary(kv => kv.Key, kv => kv.Value.Number == nodeState.Number ? nodeState : kv.Value));

        public readonly Dictionary<int, NodeState> NodeStates;
    }

    public sealed class Chain
    {
        public Chain(StepResult[] steps)
        {
            Steps = steps;
        }

        public readonly StepResult[] Steps;
    }

    public sealed class SolutionState
    {
        public SolutionState(PuzzleState puzzleState, Chain[] chains)
        {
            PuzzleState = puzzleState;
            Chains = chains;
        }

        public readonly PuzzleState PuzzleState;
        public readonly Chain[] Chains;
    }

    public sealed class StepResult
    {
        public StepResult(int stepStartNumber, int stepEndNumber, PuzzleState newState)
        {
            StepStartNumber = stepStartNumber;
            StepEndNumber = stepEndNumber;
            NewState = newState;
        }

        public readonly int StepStartNumber;
        public readonly int StepEndNumber;
        public readonly PuzzleState NewState;
    }

    public static class Puzzle
    {
        private static StepResult Step(PuzzleState puzzleState, int stepStartNumber)
        {
            // Find start node
            var stepStartNode = puzzleState.NodeStates[stepStartNumber];

            // Rotate start node clockwise
            var newStartNode =
                new NodeState(
                    stepStartNode.Number,
                    (stepStartNode.NextNeighborIndex + 1) % stepStartNode.Neighbors.Length,
                    stepStartNode.Neighbors,
                    stepStartNode.Bit);

            // Make the step
            return
                new StepResult(
                    stepStartNumber,
                    newStartNode.Neighbors[newStartNode.NextNeighborIndex],
                    puzzleState.WithNodeReplaced(newStartNode));
        }

        private static Chain BuildChainFromStart(PuzzleState puzzleState, int startNodeNumber)
        {
            var currentState = puzzleState;
            var currentNodeNumber = startNodeNumber;
            var stepResults = new List<StepResult>();

            for (var i = 0; i < 4; ++i)
            {
                var stepResult = Step(currentState, currentNodeNumber);

                stepResults.Add(stepResult);

                currentState = stepResult.NewState;
                currentNodeNumber = stepResult.StepEndNumber;
            }

            return new Chain(stepResults.ToArray());
        }

        private static IEnumerable<SolutionState> AdvanceSolutionState(SolutionState solutionState, Func<Chain, bool> isChainGood) =>
            solutionState.PuzzleState.NodeStates.Keys
                .AsParallel()
                .AsOrdered()
                .Select(chainStartNodeNumber => BuildChainFromStart(solutionState.PuzzleState, chainStartNodeNumber))
                .Where(isChainGood)
                .Select(x => new SolutionState(x.Steps.Last().NewState, solutionState.Chains.Concat(new[] {x}).ToArray()));

        public static IEnumerable<SolutionState> Solve(SolutionState initialSolutionState, int[][] tails, bool useHint3)
        {
            bool ChainMatchesTail(Chain chain, int[] tail) =>
                useHint3
                    ? chain.Steps.Skip(1).Select(stepResult => stepResult.StepEndNumber).SequenceEqual(tail) // Check for full tail match
                    : chain.Steps.Last().StepEndNumber == tail.Last(); // Only check the endpoint

            // Start with just initial solution state, then aggregate each tail onto the list of potential states.
            // AdvanceSolutionState will project each current potential solution into 0-N advanced solution states - this is how we'll branch / close off paths.
            return
                tails.Aggregate(
                    (IEnumerable<SolutionState>)new[] {initialSolutionState},
                    (accum, tail) => accum.SelectMany(solution => AdvanceSolutionState(solution, chain => ChainMatchesTail(chain, tail))));
        }
    }
}
