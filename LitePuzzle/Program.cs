﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;

namespace LitePuzzle
{
    public static class Program
    {
        private static void Main(string[] args)
        {
            // Will need this to hash answer
            var sha256 = SHA256.Create();

            // Deliberately lazy
            var solutionStates = Puzzle.Solve(Data.InitialSolutionState, Data.Hint3Tails.ToArray(), useHint3: true);

            // Going to keep a record of answers we've already tested out, there's a lot of dupes
            var attemptedAnswers = new Dictionary<string, bool>();

            var currentSolutionNumber = 1;
            foreach (var solutionState in solutionStates)
            {
                var answerBits =
                    solutionState.Chains
                        .Select(chain => Data.InitialSolutionState.PuzzleState.NodeStates[chain.Steps.First().StepStartNumber].Bit)
                        .ToArray();

                var answerBinary = string.Join("", answerBits);

                if (!attemptedAnswers.ContainsKey(answerBinary))
                {
                    var answerBytes = Util.BytesFromBinaryString(answerBinary);

                    var answerHex = Util.ByteArrayToString(answerBytes);

                    Console.WriteLine($"Attempting answer {answerHex}");

                    var answerHash = Util.ByteArrayToString(sha256.ComputeHash(answerBytes));

                    // Check against goal hash
                    var hashMatches = answerHash == Data.GoalHash;

                    attemptedAnswers.Add(answerBinary, hashMatches);
                    
                    if (hashMatches)
                    {
                        // Multiple solution states can have the same hash - this match may or may not be the one that matches the initial puzzle given.
                        // A hash match means we have the private key either way, though.

                        Console.WriteLine($"Answer hash for solution {currentSolutionNumber} matches!");
                        Console.WriteLine($"Binary: {answerBinary}");
                        Console.WriteLine($"Hex: {answerHex}");

                        // Sanity check - the solution chains' ends should match the Rings
                        Console.WriteLine($"Chains match rings: {solutionState.Chains.Select(chain => chain.Steps.Last().StepEndNumber).SequenceEqual(Data.Rings)}");

                        // Dump the state
                        Console.WriteLine("Chains:");
                        foreach (var chain in solutionState.Chains)
                        {
                            Console.WriteLine($"{solutionState.PuzzleState.NodeStates[chain.Steps.First().StepStartNumber].Bit}  {string.Join(" -> ", new[]{chain.Steps.First().StepStartNumber}.Concat(chain.Steps.Select(step => step.StepEndNumber)))}");
                        }

                        Console.WriteLine("Nodes:");
                        foreach (var nodeState in solutionState.PuzzleState.NodeStates)
                        {
                            Console.WriteLine($"Node {nodeState.Value.Number} -> {nodeState.Value.Neighbors[nodeState.Value.NextNeighborIndex]}");
                        }

                        break;
                    }
                }

                currentSolutionNumber += 1;

                // Print some progress so we know it's alive
                if (currentSolutionNumber % 5000 == 0)
                {
                    Console.WriteLine($"Solution {currentSolutionNumber}");
                }
            }
        }
    }
}
