﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LitePuzzle
{
    public static class Util
    {
        public static byte[] BytesFromBinaryString(string s)
        {
            const int numBitsInByte = 8;

            var truncatedString = s.Substring(0, s.Length - s.Length % numBitsInByte);

            IEnumerable<byte> GetBytes()
            {
                for (var i = 0; i < truncatedString.Length; i += numBitsInByte)
                {
                    var eightBits = truncatedString.Substring(i, numBitsInByte);

                    yield return Convert.ToByte(eightBits, 2);
                }
            }

            return GetBytes().ToArray();
        }

        public static string ByteArrayToString(byte[] byteArray)
        {
            var stringBuilder = new StringBuilder(byteArray.Length * 2);

            foreach (var b in byteArray)
            {
                stringBuilder.AppendFormat("{0:x2}", b);
            }

            return stringBuilder.ToString();
        }
    }
}
